var Browser = require("zombie");

// var jQuery = require("jquery");
// var jasmineJquery = require("jasmine-jquery");

var app = require("../app");
var browser = new Browser();;

describe("The frontpage", function () {
  var server;

  beforeEach(function () {
    var listening = false;
    var loaded = false;
    runs(function() {
      server = app.listen(3000, '127.0.0.1', function () { listening = true });
    });
    waitsFor(function() { return listening }, "Couldn't start server.", 500);
    runs(function() {
      browser.visit("http://localhost:3000/").then(function () { loaded = true });
    });
    waitsFor(function() { return loaded }, "Couldn't load page.", 500);
  });

  afterEach(function () { server.close() });

  it("should says that it's working", function () {
    expect(browser.text("h1")).toEqual("It works!");
  });

  it("should have status image", function () {
    expect(browser.query('img[src*="codeship.io"]')).not.toBe(undefined);
  });
});
